object Capital: TCapital
  Left = 0
  Top = 0
  Caption = 'Capital'
  ClientHeight = 134
  ClientWidth = 369
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ed_nome: TEdit
    Left = 8
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'Nome'
  end
  object ed_pop: TEdit
    Left = 8
    Top = 35
    Width = 121
    Height = 21
    TabOrder = 1
    Text = 'Popula'#231'ao'
  end
  object ed_exterritorial: TEdit
    Left = 8
    Top = 62
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'Extens'#227'o Territorial'
  end
  object ListBox_p: TListBox
    Left = 135
    Top = 8
    Width = 138
    Height = 75
    ItemHeight = 13
    TabOrder = 3
  end
  object btn_enviar: TButton
    Left = 8
    Top = 101
    Width = 75
    Height = 25
    Caption = 'Enviar'
    TabOrder = 4
  end
  object btn_atualizar: TButton
    Left = 287
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Atualizar'
    TabOrder = 5
  end
  object btn_excluir: TButton
    Left = 287
    Top = 39
    Width = 75
    Height = 25
    Caption = 'Excluir'
    TabOrder = 6
  end
  object btn_salvar: TButton
    Left = 287
    Top = 101
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 7
  end
  object ed_salvar: TEdit
    Left = 135
    Top = 101
    Width = 138
    Height = 25
    TabOrder = 8
    Text = 'Nome do arquivo'
  end
end
